# Webview业务细解

## 问题起源

我们有一些业务,如旧货车金融,可能会使用到hybird,于是考虑在RN上引入webview
```
  Warning Please use the react-native-community/react-native-webview fork of this component instead. To reduce the surface area of React Native, <WebView/> is going to be removed from the React Native core. For more information, please read The Slimmening proposal.
```
官方不再维护webview组件

引入社区维护的react-native-webview [文档地址](https://github.com/react-native-community/react-native-webview)

先入引入react-native-webview,发现在项目启动时,Rn有很多脚本直接使用yarn管理(yarn是Rn他们自己开发的).我们随着使用yarn,暂时弃用npm是因为我们很多操作需要手动

引入过程会有一些问题,如

>`Duplicate module name: react-native`
遇到此问题,检查引入两次的package.json文件,删除掉其中一份,然后清掉缓存重启
`react-native start --reset-cache`


在起服务的过程中,遇到一些问题
因为现在的手机比以前较强,不需要再携带webview,所以要用react-native link 来链接原生功能
如无链接,会有一些没找到RNCWebView等一些问题

在link的过程中,会对现在安卓,ios的文件有修改,其中有使用到了androidx,我们还需要对
   `android/gradle.properties`
   文件进行添加两行
   `android.useAndroidX=true
   android.enableJetifier=true`
   
   link完之后 ,汇总一下,有以下文件被修改
   ```
   android/app/build.gradle
   android/app/src/main/java/com/rn_app/MainApplication.java
   android/gradle.properties
   android/settings.gradle
   ios/rn_app.xcodeproj/project.pbxproj
   ```

#跳转组件navigation设计
横向比较week和hybird,RN突出的优势在跳转的时候比较圆滑

如果和以上项目同时进行,会有一个新的问题,就是webview中使用到了androidX,这个方案是谷歌为了统一以前旧有的support版本各种混乱出现.
但是后面出现的项目直接就是以support.v4或者support.v7来写的,在新的基础上我们还需要兼容旧有的代码
enableJetifier设计为false