import AsyncStorage from "@react-native-community/async-storage";

class User {

    async getPhoneNumber() {
        return await AsyncStorage.getItem("user_phone_no");
    }

    async setPhoneNumber(number) {
        await AsyncStorage.setItem("user_phone_no", number);
    }

    async getPrePayCardNo() {
        return await AsyncStorage.getItem("user_pre_card_no");
    }

    async setPrePayCardNo(value) {
        await AsyncStorage.setItem("user_pre_card_no", value);
    }

    async getUserName() {
        return await AsyncStorage.getItem("user_name");
    }

    async setUserName(name) {
        await AsyncStorage.setItem("user_name", name);
    }
}

let user = new User();

export default user;