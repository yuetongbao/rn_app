import {Dimensions, PixelRatio} from "react-native";

//自适应计算dp,以传入ui和设计稿总宽度计算
export const fixDp = (uiWidth, baseWidth) => {
    return uiWidth * Dimensions.get("window").width / (baseWidth || 375);
};

//自适应计算字号
export const fixFont = (fontSize) => {
    return Math.round(fontSize / PixelRatio.getFontScale());
};