import fixUrl from './UrlUtil';
import ToastUtil from "./ToastUtil";

const post = function (business, body) {
    return sendHandle(...arguments);
};

const get = function (url) {
    return sendHandle(...arguments);
};

const form = () => {
    //表单，待业务
};


let sendHandle = (...args) => {
    //要求均以josn格式传输才调用此接口
    let defaultOpt = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    };
    let isPost = !!args[1];
    if (isPost) {
        defaultOpt.method = "POST";
        let body = args[1];
        if (typeof body == "object") {
            defaultOpt.body = JSON.stringify(body);
        } else {
            defaultOpt.body = body;
        }
    }
    return new Promise((resolve, reject) => {
        let target = isPost ? fixUrl(args[0]) : args[0];
        console.log(target, 1);
        fetch(target, defaultOpt)
            .then((response) => response.json())
            .then((responseJson) => {
                resolve(responseJson);
            })
            .catch((error) => {
                reject(error);
            });
    });
};

export default {
    post,
    get
};