import {isDev, isTest, testHost, productionHost} from "../constants/Vaules";

let domainUrl = isTest ? testHost : productionHost;
const environmentUrl = (business) => {
    let requireUrl = domainUrl + (isTest ? '/' + (isDev ? 'dev' : '') + 'gwzfpay/' : '/wxweb/');
    return requireUrl + business;
};
export default environmentUrl;