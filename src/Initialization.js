import App from "./containers/Entry";
import React from "react";
import SplashScreen from "react-native-splash-screen";

class Initialization extends React.Component {
    render() {
        return <App/>;
    }

    componentDidMount(): void {
        SplashScreen.hide();//手动隐藏
    }
}

export default Initialization;