import React, {Component} from "react";
import {View, Text, Button, Image} from "react-native";
import {RouteName} from "../../constants/RouteConfig";
import ToastUtil from "../../utils/ToastUtil";
import user from "../../common/User";

export default class ShowParam extends Component {
    static navigationOptions = {
        title: "演示",
        tabBarVisible: false
    };

    render() {
        /* 2. Get the param, provide a fallback value if not available */
        const {navigation} = this.props;
        const itemId = navigation.getParam("itemId", "NO-ID");
        const myName = navigation.getParam("name", "some default value");

        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                <Text>ShowParam Screen</Text>
                <Text>itemId: {JSON.stringify(itemId)}</Text>
                <Text>otherParam: {JSON.stringify(myName)}</Text>

                <Button
                    title="Go to This Page... again"
                    onPress={() => {
                        // this.props.navigation.pop();
                        // this.props.navigation.push('ShowParam', {
                        //     itemId: Math.floor(Math.random() * 100),
                        // });
                        //刷新当前页面,加载不同的数据,这个行为不会叠加在栈上,如果需要叠加,使用push
                        this.props.navigation.navigate(RouteName.ShowParam, {
                            itemId: Math.floor(Math.random() * 100)
                        });
                    }
                    }
                />
                <Button
                    title="Go back"
                    onPress={() => this.props.navigation.goBack()}
                />
            </View>
        );
    }

    componentDidMount(): void {
    }

    componentWillUnmount(): void {
        //在此调试代码
        ToastUtil.showLong("exit param ~", 1);
    }
};