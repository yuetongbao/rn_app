import React from "react";
import {View, Button, Text, StyleSheet, Image} from "react-native";
import {RouteName} from "../../constants/RouteConfig";
import user from "../../common/User";
import SvgUri from "react-native-svg-uri";
import {fixDp, fixFont} from "../../common/Global";

const homeStyle = StyleSheet.create({
    titleBox: {flexDirection: "row", justifyContent: "space-between", paddingLeft: 20, paddingRight: 21, height: 44, alignItems: "flex-end"},
    title: {fontSize: fixFont(22), fontWeight: "bold"},
    promoBox: {},
    promoImg: {width: fixDp(340), height: fixDp(110)},
    blocksBox: {flexDirection: "row"},
    block: {width: fixDp(10), height: fixDp(3), backgroundColor: "green", marginLeft: 5, marginRight: 5},
    block_gray: {backgroundColor: "gray"},
    businessPart: {flexDirection: "row", marginTop: fixDp(24), marginBottom: fixDp(26)},
    businessBox: {marginLeft: fixDp(15), marginRight: fixDp(15), alignItems: "center"},
    businessText: {fontSize: fixFont(14), marginTop: fixDp(10)}
});

class Home extends React.Component {
    render() {

        return (
            <View>
                <View style={homeStyle.titleBox}>
                    <Text style={homeStyle.title}>粤通宝</Text>
                    <SvgUri width={fixDp(18)} height={fixDp(23)} source={require("../../assets/img/home/bell.svg")}/>
                </View>
                <View style={{alignItems: "center"}}>
                    <View style={{paddingTop: fixDp(23), paddingBottom: fixDp(11)}}>
                        <Image style={homeStyle.promoImg} source={require("../../assets/img/banner/banner1.png")}/>
                    </View>
                    <View style={homeStyle.blocksBox}>
                        <View style={homeStyle.block}/>
                        <View style={[homeStyle.block, homeStyle.block_gray]}/>
                        <View style={[homeStyle.block, homeStyle.block_gray]}/>
                    </View>
                    <View style={homeStyle.businessPart}>
                        <View style={homeStyle.businessBox}>
                            <Image style={{width: fixDp(48), height: fixDp(48)}} source={require("../../assets/img/home/business1.png")}/>
                            <Text style={homeStyle.businessText}>新卡申办</Text>
                        </View>
                        <View style={homeStyle.businessBox}>
                            <Image style={{width: fixDp(48), height: fixDp(48)}} source={require("../../assets/img/home/business2.png")}/>
                            <Text style={homeStyle.businessText}>产品激活</Text>
                        </View>
                        <View style={homeStyle.businessBox}>
                            <Image style={{width: fixDp(48), height: fixDp(48)}} source={require("../../assets/img/home/business3.png")}/>
                            <Text style={homeStyle.businessText}>一键绑卡</Text>
                        </View>
                        <View style={homeStyle.businessBox}>
                            <Image style={{width: fixDp(48), height: fixDp(48)}} source={require("../../assets/img/home/business4.png")}/>
                            <Text style={homeStyle.businessText}>充值/余额</Text>
                        </View>
                    </View>

                </View>
            </View>);
    }

    componentDidMount(): void {

    }

    componentWillUnmount(): void {
        //在此调试代码
    }

}

export default Home;