import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class Home extends Component {
    render() {
        return (
            <View style={{alignItems: 'center', justifyContent: 'center', textAlign: 'center', margin: 10}}>
                <Text>
                    在 web 浏览器中, 你可以使用 a 标签作为锚点，链接到不同的页面。 当用户单击某个链接时, 该 URL 就会被推送到浏览器历史记录堆栈。 当用户点击返回按钮时,
                    浏览器会从历史堆栈顶部删除正在访问的页面, 因此当前页现在就成了以前访问过的页面。 React Native没有像Web浏览器那样的内置全局历史堆栈的想法 -这就是 React Navigation
                    存在的意义
                </Text>
            </View>
        );
    }
};