import React, {Component} from 'react';
import {View, Button, Text} from 'react-native';

export default class ButtonTest extends Component {
    timer: Number;
    constructor(props) {
        super(props);
        this.state = {rData: "initTx"};
        this.timer = setInterval(() => {
            this.setState(previousState => {
                return {rData: Math.random() * 1000 >> 0};
            });
        }, 1000);
    }
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <Button onPress={this.onPressIt.bind(this)}
                        title="暂停随机"
                        color="#841584"
                />
                <Text>{this.state.rData}</Text>
            </View>
        );
    }
    onPressIt() {
        clearInterval(this.timer);
        this.setState({rData: "Timer Stop!"});
        // this.setState(()=> { return {rData: "Timer Stop By Function!"}});
    }
};