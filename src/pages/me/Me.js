import React from "react";
import {View, Text, StyleSheet, Button} from "react-native";
import RequestUtil from "../../utils/RequestUtil";
import Loading from "../../components/Loading";
import ToastUtil from "../../utils/ToastUtil";

class Me extends React.Component {
    constructor(props) {
        super(props);
        this.state = {result: "", result2: "", loading: false}
    }

    name: "network_do";

    render() {
        return (
            <View style={fillStyle.container}>
                <Text style={fillStyle.word}>占用文本，暂无定义</Text>
                <Button onPress={this.onPressIt.bind(this)} title="简单Get请求" color="#841584" />
                <Button onPress={this.onPressBusiness.bind(this)} title="查询WxfQryCarOrd" color="#841584" />
                <Button onPress={this.onPressMsgSend.bind(this)} title="下发信息WxfSendSms" color="#841584" />
                <Button onPress={this.onPressLogin.bind(this)} title="登录WxfLogin" color="#841584" />
                <Text style={fillStyle.word_small}>{this.state.result}</Text>
                <Text style={fillStyle.word_small}>{this.state.result2}</Text>
                <Loading visible={this.state.loading} color="#5D93E9" size="small"/>
            </View>

        )
    }

    onPressIt() {
        let url = 'https://facebook.github.io/react-native/movies.json';
        RequestUtil.get(url).then((data) => {
            this.setState({result: JSON.stringify(data), loading: false});
        }, (err) => {
            ToastUtil.showLong(err);
        }).catch((err) => {
            ToastUtil.showLong(err);
        })
    }

    onPressBusiness(){
        let business = 'WxfQryCarOrd.wxf';
        RequestUtil.post(business, {"mbl_no":"18200740514"}).then((data) => {
            this.setState({result: JSON.stringify(data), loading: false});
        }, (err) => {
            ToastUtil.showLong(err);
        }).catch((err) => {
            ToastUtil.showLong(err);
        });
    }

    onPressMsgSend(){
        let business = 'WxfSendSms.wxf';
        RequestUtil.post(business, {"mbl_no":"18200740514"}).then((data) => {
            this.setState({result: JSON.stringify(data), loading: false});
        }, (err) => {
            ToastUtil.showLong(err);
        }).catch((err) => {
            ToastUtil.showLong(err);
        });
    }

    onPressLogin(){
        let business = 'WxfLogin.wxf';
        RequestUtil.post(business, {"mbl_no":"18200740514", "sms_cd":"123456"}).then((data) => {
            this.setState({result: JSON.stringify(data), loading: false});
        }, (err) => {
            ToastUtil.showLong(err);
        }).catch((err) => {
            ToastUtil.showLong(err);
        });
    }

}

const fillStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    word: {
        textAlign: "center",
        fontSize: 30,
        margin: 10,
        color: 'red'
    },
    word_small: {
        textAlign: "center",
        fontSize: 20,
        padding: 10,
        color: 'blue'
    }
});

export default Me