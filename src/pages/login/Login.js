import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {RouteName} from "../../constants/RouteConfig";

export default class Home extends Component {
    static navigationOptions = {title: '登录'};
    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={{textAlign: 'center'}}>登录面板</Text>
                <Button onPress={() => this.props.navigation.navigate(RouteName.ShowParam, {
                    itemId: 100,
                    name: "test_text"
                })} title="参数面板"/>
            </View>
        );
    }
};