import React from "react";
import {createBottomTabNavigator, createAppContainer, createStackNavigator} from "react-navigation";

/*
* 如果有关跳转,所有页面在此注册相关路由
*
* */
import Main from "./MainPage";
import Card from "./CardPage";
import Shop from "./ShopPage";
import Me from "./MimePage";
import ShowText from "../pages/home/ShowText";
import ButtonTest from "../pages/home/ButtonTest";
import ShowParam from "../pages/home/ShowParam";
import Login from "../pages/login/Login";
import {Image, StyleSheet} from "react-native";
import SvgUri from "react-native-svg-uri";
import {fixDp} from "../common/Global";

//每个容器自己管理Stack,如不同的容器跳转到同样的页,那么需要重复配置
const MainStack = createStackNavigator({
        Home: {screen: Main},
        ShowText: {screen: ShowText},
        ButtonTest: {screen: ButtonTest},
        ShowParam: {screen: ShowParam},
        Login: {screen: Login}
    },
    {
        // headerMode: 'none',
        // navigationOptions: {title: '首页'} //navigationOptions的stack配置可与bottom的draw配置融合
    });

//如果是在stack的第二层以上的展示页面，隐藏掉下面的tarBar
MainStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }
    return {
        tabBarVisible
    };
};

const CardStack = createStackNavigator({
        Card: {screen: Card},
        ShowParam: {screen: ShowParam}
    },
    {
        mode: "modal",
        headerMode: "none"
    });

const barStyle = StyleSheet.create({
    stretch: {
        width: 30,
        height: 30
    }
});

const TabNavigator = createBottomTabNavigator({
    MainPage: {
        screen: MainStack, navigationOptions: {
            title: "首页", tabBarIcon: ({focused}) => {
                let url = focused ? require("../assets/img/tabbar/home.svg") : require("../assets/img/tabbar/home_gray.svg");
                return <SvgUri key="home" width={fixDp(17)} height={fixDp(17)} source={url}/>;
            }
        }

    },
    CardPage: {
        screen: CardStack, navigationOptions: {
            title: "服务", tabBarIcon: ({focused}) => {
                let url = focused ? require("../assets/img/tabbar/server.svg") : require("../assets/img/tabbar/server_gray.svg");
                return <SvgUri key="server" width={fixDp(17)} height={fixDp(17)} source={url}/>;
            }
        }
    },
    ShopPage: {
        screen: Shop, navigationOptions: {
            title: "商店", tabBarIcon: ({focused}) => {
                let url = focused ? require("../assets/img/tabbar/shop.svg") : require("../assets/img/tabbar/shop_gray.svg");
                return <SvgUri key="shop" width={fixDp(17)} height={fixDp(17)} source={url}/>;
            }
        }
    },
    MimePage: {
        screen: Me, navigationOptions: {
            title: "我的", tabBarIcon: ({focused}) => {
                let url = focused ? require("../assets/img/tabbar/mine.svg") : require("../assets/img/tabbar/mine_gray.svg");
                return <SvgUri key="mine" width={fixDp(19)} height={fixDp(19)} source={url}/>;
            }
        }
    }
}, {
    tabBarOptions: {
        //当前选中的tab bar的文本颜色和图标颜色
        activeTintColor: "#29B455",
        //当前未选中的tab bar的文本颜色和图标颜色
        inactiveTintColor: "#3C3F3C",
        //是否显示tab bar的图标，默认是false
        showIcon: true,
        //showLabel - 是否显示tab bar的文本，默认是true
        showLabel: true,
        //是否将文本转换为大小，默认是true
        upperCaseLabel: false,
        //material design中的波纹颜色(仅支持Android >= 5.0)
        // pressColor: '#788493',
        //按下tab bar时的不透明度(仅支持iOS和Android < 5.0).
        pressOpacity: 0.8,
        //tab bar的样式
        style: {
            // backgroundColor: "#fff",
            paddingBottom: 4,
            paddingTop: 4,
            borderTopWidth: 0.1, //最顶上的那条线
            // borderTopColor: "#ccc"
        },
        //tab bar的文本样式
        labelStyle: {
            fontSize: 10
            // margin: 2
        },
        tabStyle: {
            // height: 42
        }
        //tab 页指示符的样式 (tab页下面的一条线).
        // indicatorStyle: {height: 0}
    },
    //tab bar的位置, 可选值： 'top' or 'bottom'
    tabBarPosition: "bottom",
    //是否允许滑动切换tab页
    swipeEnabled: true,
    //是否在切换tab页时使用动画
    animationEnabled: false,
    //是否懒加载
    lazy: true,
    //返回按钮是否会导致tab切换到初始tab页？ 如果是，则设置为initialRoute，否则为none。 缺省为initialRoute。
    // backBehavior: "none",
    initialRouteName: "MainPage"
});

export default createAppContainer(TabNavigator);