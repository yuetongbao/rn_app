import React from 'react';
import {WebView} from 'react-native-webview';
import {View,StyleSheet} from "react-native";
import Loading from "../components/Loading";

class CardPage extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'webview'
    });

    constructor(props){
        super(props);
        this.state = {loading:false}
    }

    render() {
        return (
            <View style={styles.container}>
                <WebView source={{uri: 'https://www.qq.com/'}} onLoadStart={this.onLoadBegin.bind(this)} onLoadEnd={this.onLoadDone.bind(this)} />
                <Loading visible={this.state.loading} color="#5D93E9" size="small"/>
            </View>
        );
    }

    onLoadDone(){
        this.setState({loading:false})
    }

    onLoadBegin(){
        this.setState({loading:true})
    }
}

//样式定义
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

export default CardPage;