import React from 'react';
import Me from "../pages/me/Me";

class MineContainer extends React.Component {
    componentDidMount() {
    }

    render() {
        return <Me {...this.props} />;
    }
}

export default MineContainer
