import React from "react";
import About from "../pages/about/About";
import {Image} from "react-native";

class MainContainer extends React.Component {
    static navigationOptions = {
        title: "商城",
    };

    render() {
        return <About {...this.props} />;
    }
}

export default MainContainer;
