import React from 'react';
import {BackHandler, ToastAndroid} from 'react-native';
import Home from "../pages/home/Home";

class MainContainer extends React.Component {
    static navigationOptions = {
        header: null, //在些去除默认页的头
        title: '首页' //这个不是底部的配置，是顶部title的配置，如果header不为null生效
    };

    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props) {
        super(props);
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid)
        );
    }

    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid)
        );
    }

    componentWillUnmount() {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }


    render() {
        return <Home {...this.props} />;
    }

    onBackAndroid = () => {
        //详情见https://reactnavigation.org/docs/zh-Hans/custom-android-back-button-handling.html
        if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
            //最近2秒内按过back键，可以退出应用。
            BackHandler.exitApp();
            return false;
        }
        this.lastBackPressed = Date.now();
        ToastAndroid.show('再按一次退出粤通宝', ToastAndroid.SHORT);
        // return true;
        return true;
    };
}

export default MainContainer
