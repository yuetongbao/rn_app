#粤通宝App项目前期React Native知识准备

## 代码部分
##### RN代码中大量使用了ES6语法  
这部分只需要重点掌握，读别人的代码前必备工作。
```
* let、const  
* import、export
* class、extends、super
* promise
* arrow functions（箭头函数）
```
ES6语法规范较多，掌握上述知识运用即可

###Js辅助工具库
`Lodash` 是一套工具库，它内部封装了诸多对字符串、数组、对象等常见数据类型的处理函数,其中部分是目前ECMAScript尚未制订的规范，但同时被业界所认可的辅助函数。  
App项目将会引入此库，以提高生产效率
[中文文档链接](https://www.lodashjs.com/docs/latest)

---

## 框架部分

**中文文档**
https://reactnative.cn/docs/animations/
入门部分需要掌握

**ReactNative运行原理**  
RN运行需要先加载jsbundle文件到内存，进行解析，最后再渲染到UI层。从渲染的角度，RN是原生控件渲染出来的，这跟H5通过webView渲染有区别

**ReactJs和ReactNative的区别**  
标签不同，如react支持的标签是\<div> ,ReactNative支持的是\<View>  
RN支持的style属性就仅仅是原生组件所支持的所有属性  
注意不等同CSS3, 如float，margin:auto, 这些无效
[可支持属性列表](https://reactnative.cn/docs/layout-props/)

**生命周期**

+ 实例化（初始化，只执行一次）
  + constructor 设置默认的props->设置默认的state
  + componentWillMount 完成渲染之前执行，此时可以设置state
  + render 创建虚拟DOM，此时不能修改state
  + componentDidMount 真实的DOM渲染完毕，此时可以更改组件props及state
+ 存在期：(这个时候的主要行为是状态的改变导致组件更新)
  + componentWillReceiveProps 组件接收到新的props,此时可以更改组件props及state
  + shouldComponentUpdate 操作组件是否应当渲染新的props或state，返回布尔值，首次渲染该方法不会被调用。
  + componentWillUpdate 接收到新的props或者state后，进行渲染之前调用，此时不允许更新props或state。
  + render 创建（更新）虚拟DOM
  + componentDidUpdate 组件真实的DOM更新完成
+ 销毁期：
  + componentWillUnmount 组件被移除之前，主要用于做一些清理工作，比如事件监听

react的语法 ＋ native的组件

RN布局  
布局在移动开发中占据重要位置，RN [官方文档](https://reactnative.cn/docs/flexbox/)讲得比较简单，真要学习先要掌握CSS3的flex布局，RN只是它的一个简化版本 [详见CSS3 Flex布局](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)
前期基本点掌握即可,如按百分比平分{flex:1},{flex:2}

自适应布局方案
自适应布局方案采用了，将 UI 等比放大到 app 上的自适应布局。

UI默认给多套图,我们按安卓dp的设计方案，采用 pxToDp 函数，即可将 UI 等比放大到 Android 机器上。

```javascript
import {Dimensions} from "react-native";

//自适应计算dp,以传入ui和设计稿总宽度计算,也可以按750设计稿传入
export const fixDp = (uiWidth, baseWidth) => {
    return uiWidth * Dimensions.get("window").width / (baseWidth || 375);
};
```
调用方法
import {fixDp} from "../common/Global";
...
return <SvgUri key="server" width={fixDp(17)} height={fixDp(17)} source={url}/>;
...

setState
+ 一切界面变化都是状态state变化 
+ state的修改必须通过setState()方法
  + this.state.likes = 100; // 这样的直接赋值修改无效！
  + setState 是一个 merge 合并操作，只修改指定属性，不影响其他属性
  + setState 是异步操作，修改不会马上生效

网络传输
React Native提供了和web标准一致的[Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
```javascript
var options = {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    firstParam: 'yourValue',
    secondParam: 'yourOtherValue',
  })
};
fetch(url, options);
```

跳转带参数


异步持久化存储
类似于localStorage
https://github.com/react-native-community/async-storage
注意这个库是异步的,简单用法如下：
```javascript
class User{
    async getPhoneNumber() {
        return await AsyncStorage.getItem("user_phone_no");
    }
    
    async setPhoneNumber(number) {
        await AsyncStorage.setItem("user_phone_no", number);
    }
}
let user = new User();
user.setPhoneNumber("23121212").then(()=>{
    user.getPhoneNumber().then((d)=>{
        ToastUtil.showShort(d)
    })
})
```

引入svg支持  
https://www.npmjs.com/package/react-native-svg  
这个库支持svg, {
               Circle,
               Ellipse,
               G,
               Text,
               TSpan,
               TextPath,
               Path,
               Polygon,
               Polyline,
               Line,
               Rect,
               Use,
               Image,
               Symbol,
               Defs,
               LinearGradient,
               RadialGradient,
               Stop,
               ClipPath,
               Pattern,
               Mask,
           }  
仅是简单的路径之类满足，如果是svg文件，那么需要引入库  
https://www.npmjs.com/package/react-native-svg-uri  
由于美术那边使用了一个叫做Sketch的软件，设计的时候莫名多出一些 Text elements / transform属性，注释，中文等
详见问题描述  
https://github.com/vault-development/react-native-svg-uri/pull/100  
现fork一个新的库，项目自己使用，详见package.json

## 环境搭建
1.jdk 编译原生&第三方库代码  
2.android-studio 当出现问题时,能通过此工具快速排查
>主要解决一些包丢失,gradle配置等,还包括logcat日志等  

3.android-sdk
>需要配置环境变量到此目录  
>此目录下的./platform-tools(adb工具的使用)  
>此目录下的模拟器./emulator  

4.nodejs环境,安装时已捆绑安装npm包管理工具,额外安装yarn包管理工具
```
代码通过 包（package） (或者称为 模块（module）) 的方式来共享。 一个包里包含所有需要共享的代码，以及描述包信息的文件，称为 package.json 。
```

5.webstorm  
专业前端代码编辑器,代替的方案也有vscode,代码错误提示、Js-ESLint规范和插件等 


## 调试
当模块功能代码写好以后,主要有两个途径调试  
+ 真机调试 (usb调试 adb devices只保留一部机器, 如有多台机器连接，多台手机一齐启动)
+ 模拟器调试
    - emulator 原生自带的安卓模拟器,需要AS搭配操作,不然要面对较为复杂的命令行&参数
    - genymotion [链接](http://www.genymotion.net/) 国外比较强大的模拟器,个性化较强
    - 夜神模拟器 中国游戏玩家专用,较强,简单安装,目录下的nox_adb.exe能做到adb的所有事情,问题是它的安卓API可能版本有点旧

ios需要专门的mac机器开发,暂不考虑在此文档讲述

## 技术预研
https://www.npmjs.com/package/react-native-image-picker  
可以开启相机，打开相册,应用于读取本地图片文件
适用于上传前压缩


**项目结构**

```
├── README.md                   // 文档,凡以.md结尾的都是文档,类此
├── src
│     ├─assets                  // 资源管理
│     │  └─img
│     │      └─tabbar
│     ├─components              //公用组件
│     ├─constants               //常量&配置
│     ├─containers              //模块聚合页&路由配置
│     ├─pages                   //具体业务模块
│     │  ├─about
│     │  ├─home
│     │  ├─login
│     │  ├─me
│     │  └─splash
│     └─utils
├── ios                         // ios原生部分
├── index.js                    // 项目注册入口文件
├── android                     // android原生部分
├── node_modules                // 项目依赖包
├── __test__                    // 自动化测试
├── package.json                // 项目配置信息
├── .babelrc                    // 设置转码的规则,插件,文件地址映射
├── .eslintrc                   // 代码校验规则配置
└── yarn.lock                   // 依赖的版本信息管理
```