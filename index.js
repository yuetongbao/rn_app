import {AppRegistry} from 'react-native';
import App from './src/Initialization';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
